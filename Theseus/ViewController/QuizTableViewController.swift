//
//  QuizTableViewController.swift
//  Theseus
//
//  Created by Lukas Gauster on 08/02/2019.
//  Copyright © 2019 Mirko Ricci. All rights reserved.
//

import UIKit

protocol QuizTableViewCellDelegate {
    func updatedQuestion(withId questionId: String, withAnswer answer: Int)
}

class QuizTableViewController: UITableViewController {
    
    let kGradeTableViewCellIdentifier = "GradeTableViewCell"
    let kPersonalityTableViewCellIdentifier = "PersonalityTableViewCell"
    let kInterestTableViewCellIdentifier = "InterestTableViewCell"
    
    var allQuestions = [QuizQuestion]()
    var quizType: QuizType = .interests
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset = UIEdgeInsets(top: 18, left: 0, bottom: 0, right: 0)
        self.tableView.register(UINib(nibName: "GradeTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: kGradeTableViewCellIdentifier)
        self.tableView.register(UINib(nibName: "PersonalityTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: kPersonalityTableViewCellIdentifier)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allQuestions.count
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64.0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch quizType {
        case .personality:
            if let cell = tableView.dequeueReusableCell(withIdentifier: kPersonalityTableViewCellIdentifier, for: indexPath) as? PersonalityTableViewCell {
                cell.question = allQuestions[indexPath.row]
                cell.quizQuestionCellDelegate = self
                return cell
            }
        case .interests:
            let cell = tableView.dequeueReusableCell(withIdentifier: kInterestTableViewCellIdentifier, for: indexPath)
            
            if let label = cell.viewWithTag(101) as? UILabel {
                label.text = allQuestions[indexPath.row].question
            }
            
            if let backgroundView = cell.viewWithTag(102) {
                backgroundView.layer.cornerRadius = 10.0
                backgroundView.layer.applySketchShadow(color: UIColor(rgba: "#000000"), alpha: 0.5, x: 3, y: 3, blur: 4, spread: 0)
            }
            
            if let imageView = cell.viewWithTag(103) as? UIImageView {
                if allQuestions[indexPath.row].answer == 0 {
                    imageView.isHidden = true
                } else {
                    imageView.isHidden = false
                }
            }
            
            return cell
        case .grades:
            if let cell = tableView.dequeueReusableCell(withIdentifier: kGradeTableViewCellIdentifier, for: indexPath) as? GradeTableViewCell {
                cell.question = allQuestions[indexPath.row]
                cell.quizQuestionCellDelegate = self
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch quizType {
        case .interests:
            // Toggle checkmark            
            tableView.deselectRow(at: indexPath, animated: true)
            if allQuestions[indexPath.row].answer == 0 {
                allQuestions[indexPath.row].answer = 1
            } else {
                allQuestions[indexPath.row].answer = 0
            }
            
            tableView.reloadRows(at: [indexPath], with: .automatic)
        default:
            break
        }
    }
}

extension QuizTableViewController: QuizTableViewCellDelegate {
    func updatedQuestion(withId questionId: String, withAnswer answer: Int) {
        // Update the answer to the question
        if let index = allQuestions.index(where: { (question) -> Bool in
            question.questionId == questionId
        }) {
            allQuestions[index].answer = answer
        }
    }
}

// Save current quiz results
extension QuizTableViewController {
    func saveQuizAnswers() {
        // Save quiz to current user profile
        AppDataManager.sharedInstance.updateCurrentUserProfile(questions: allQuestions, quizType: quizType)
    }
}
