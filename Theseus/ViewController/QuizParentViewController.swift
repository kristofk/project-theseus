//
//  QuizParentViewController.swift
//  Theseus
//
//  Created by Lukas Gauster on 08/02/2019.
//  Copyright © 2019 Mirko Ricci. All rights reserved.
//

import UIKit

protocol QuizParentViewControllerDelegate {
    func didSaveQuiz(ofType type: QuizType)
    func didCancelQuiz()
}

class QuizParentViewController: UIViewController {
    
    let kHeightOfHeaderView: CGFloat = 233
    
    @IBOutlet weak var quizCategoryIconImageView: UIImageView!
    @IBOutlet weak var quizCategoryTitleLabel: UILabel!
    @IBOutlet weak var quizHeaderView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
        {
        didSet {
            confirmButton.layer.applySketchShadow(color: UIColor(rgba: "#000000"), alpha: 0.5, x: 3, y: 3, blur: 4, spread: 0)
            confirmButton.layer.cornerRadius = 10.0
        }
    }
    @IBOutlet weak var cancelButton: UIButton! {
        didSet {
            cancelButton.layer.applySketchShadow(color: UIColor(rgba: "#000000"), alpha: 0.5, x: 3, y: 3, blur: 4, spread: 0)
            cancelButton.layer.cornerRadius = 10.0
        }
    }
    
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    var quizType: QuizType = .personality
    var delegate: QuizParentViewControllerDelegate?
    
    var quizTableViewController: QuizTableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupHeaderView()
        setupConfirmButton()
        
        // Generate questions and show them when ready
        quizTableViewController?.allQuestions = QuizManager.startQuizOf(type: quizType)
        quizTableViewController?.tableView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setupHeaderView() {
        
        // Set height of header view depending on device
        let window = UIApplication.shared.windows.first
        if let topPadding = window?.safeAreaInsets.top {
            headerViewHeightConstraint.constant = kHeightOfHeaderView + topPadding
        }
        
        // Change content depending on quiz type
        switch quizType {
        case .personality:
            quizCategoryTitleLabel.text = NSLocalizedString("QuizTitlePersonality", comment: "Title of the quiz screen for the personality quiz.")
            quizHeaderView.backgroundColor = GlobalConstants.Colors.PersonalityColor
            quizCategoryIconImageView.image = UIImage(named: "personality-quiz")
            
        case .grades:
            quizCategoryTitleLabel.text = NSLocalizedString("QuizTitleGrades", comment: "Title of the quiz screen for the grade quiz.")
            quizHeaderView.backgroundColor = GlobalConstants.Colors.GradesColor
            quizCategoryIconImageView.image = UIImage(named: "grades-quiz")
            
        case .interests:
            quizCategoryTitleLabel.text = NSLocalizedString("QuizTitleInterests", comment: "Title of the quiz screen for the interests quiz.")
            quizHeaderView.backgroundColor = GlobalConstants.Colors.InterestsColor
            quizCategoryIconImageView.image = UIImage(named: "interests-quiz")
        }
        
    }
    
    private func setupConfirmButton() {
        // Change button background image depending on quiz type
        switch quizType {
        case .personality:
            confirmButton.backgroundColor = GlobalConstants.Colors.PersonalityColor
        case .grades:
            confirmButton.backgroundColor = GlobalConstants.Colors.GradesColor
        case .interests:
            confirmButton.backgroundColor = GlobalConstants.Colors.InterestsColor
        }
    }
    
    
    @IBAction func didPressDismissButton(_ sender: Any) {
        delegate?.didCancelQuiz()
    }
    
    
    @IBAction func didPressConfirmButton(_ sender: Any) {
        // Save answers and inform the delegate
        quizTableViewController?.saveQuizAnswers()
        delegate?.didSaveQuiz(ofType: quizType)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "QuizTableViewController" {
            if let targetViewController = segue.destination as? QuizTableViewController {
                quizTableViewController = targetViewController
                targetViewController.quizType = quizType
            }
        }
    }

}
