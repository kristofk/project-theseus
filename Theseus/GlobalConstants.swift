//
//  GlobalConstants.swift
//  Theseus
//
//  Created by Lukas Gauster on 08/02/2019.
//  Copyright © 2019 Mirko Ricci. All rights reserved.
//

import UIKit

struct GlobalConstants {
    
    struct Colors {
        static let PersonalityColor = UIColor(rgba: "#85CEC6")
        static let GradesColor = UIColor(rgba: "#0061AF")
        static let InterestsColor = UIColor(rgba: "#002E5E")
        static let DefaultGrayColor = UIColor(rgba: "#C5C5C5")
    }
    
}
