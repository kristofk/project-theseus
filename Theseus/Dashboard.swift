//
//  Dashboard.swift
//  Theseus
//
//  Created by Mirko Ricci on 04/02/2019.
//  Copyright © 2019 Mirko Ricci. All rights reserved.
//

import UIKit

class Dashboard: UIViewController {
 //test
    @IBOutlet var doPersonalityTest: UIButton! {
        didSet {
            doPersonalityTest.setTitle(NSLocalizedString("DoPersonalityTest", comment:"Title of the button for taking the personality test"), for: .normal)
            
        }
    }
    
    @IBOutlet var InsertGrades: UIButton! {
        
        didSet {
            InsertGrades.setTitle(NSLocalizedString("InsertGrades", comment:"Title of the button for insert the Grades"), for: .normal)
        }
    }
    
    @IBOutlet var InsertInterests: UIButton! {
        didSet {
            InsertInterests.setTitle(NSLocalizedString("InsertGrades", comment:"Title of the button for select the Interests"), for: .normal)
    }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showGradesScreen" {
            
            if let targetViewController = segue.destination as? QuizParentViewController {
                targetViewController.quizType = .grades
                targetViewController.delegate = self
            }
            
        } else if segue.identifier == "showPersonalityScreen" {
            
            if let targetViewController = segue.destination as? QuizParentViewController {
                targetViewController.quizType = .personality
                targetViewController.delegate = self
            }
            
        } else if segue.identifier == "showInterestScreen" {
            
            if let targetViewController = segue.destination as? QuizParentViewController {
                targetViewController.quizType = .interests
                targetViewController.delegate = self
            }
            
        }
    }

}

extension Dashboard: QuizParentViewControllerDelegate {
    func didCancelQuiz() {
        // Do stuff
        self.dismiss(animated: true) {
            //
        }
    }
    
    func didSaveQuiz(ofType type: QuizType) {
        // Do Stuff
        self.dismiss(animated: true) {
            //
        }
    }
}
