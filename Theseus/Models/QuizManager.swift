//
//  QuizManager.swift
//  Theseus
//
//  Created by Lukas Gauster on 08/02/2019.
//  Copyright © 2019 Mirko Ricci. All rights reserved.
//

import Foundation

public enum QuizType {
    case personality
    case grades
    case interests
}

public class QuizManager {
    
    class func startQuizOf(type: QuizType) -> [QuizQuestion] {
        return prepareQuestionsWith(quizType: type)
    }
    
    class private func prepareQuestionsWith(quizType: QuizType) -> [QuizQuestion] {
        
        var allQuestions = [QuizQuestion]()
        
        // TODO: Do this based on the current language of the user!
        switch quizType {
        case .personality:
            allQuestions.append(
                QuizQuestion(questionId: "1", question: "Do you like routines?", answer: -1))
            allQuestions.append(
                QuizQuestion(questionId: "2", question: "Do you have a good memory?", answer: -1))
            allQuestions.append(
                QuizQuestion(questionId: "3", question: "Could you sit for several hours?", answer: -1))
            allQuestions.append(
                QuizQuestion(questionId: "4", question: "Could you walk for several hours?", answer: -1))
            allQuestions.append(
                QuizQuestion(questionId: "5", question: "Do you like to take up responsibility?", answer: -1))
            allQuestions.append(
                QuizQuestion(questionId: "6", question: "Can you work good under pressure?", answer: -1))
            allQuestions.append(
                QuizQuestion(questionId: "7", question: "Are you good with numbers?", answer: -1))
            allQuestions.append(
                QuizQuestion(questionId: "8", question: "Do you like working with computers?", answer: -1))
            allQuestions.append(
                QuizQuestion(questionId: "9", question: "Do you like working with people?", answer: -1))
            allQuestions.append(
                QuizQuestion(questionId: "10", question: "Do you enjoy contact with customers?", answer: -1))
            
        case .grades:
            allQuestions.append(
                QuizQuestion(questionId: "1", question: "Maths", answer: 5))
            allQuestions.append(
                QuizQuestion(questionId: "2", question: "Business", answer: 5))
            allQuestions.append(
                QuizQuestion(questionId: "3", question: "Engineering", answer: 5))
            allQuestions.append(
                QuizQuestion(questionId: "4", question: "English", answer: 5))
            allQuestions.append(
                QuizQuestion(questionId: "5", question: "Sports", answer: 5))
            allQuestions.append(
                QuizQuestion(questionId: "6", question: "History", answer: 5))
            allQuestions.append(
                QuizQuestion(questionId: "7", question: "Spanish", answer: 5))
            allQuestions.append(
                QuizQuestion(questionId: "8", question: "Geography", answer: 5))
            allQuestions.append(
                QuizQuestion(questionId: "9", question: "Physics", answer: 5))
            allQuestions.append(
                QuizQuestion(questionId: "10", question: "Biology", answer: 5))
            
        case .interests:
            allQuestions.append(
                QuizQuestion(questionId: "1", question: "Reading", answer: 0))
            allQuestions.append(
                QuizQuestion(questionId: "2", question: "Music", answer: 0))
            allQuestions.append(
                QuizQuestion(questionId: "3", question: "Outdoors", answer: 0))
            allQuestions.append(
                QuizQuestion(questionId: "4", question: "People", answer: 0))
            allQuestions.append(
                QuizQuestion(questionId: "5", question: "Quietness", answer: 0))
            allQuestions.append(
                QuizQuestion(questionId: "6", question: "Sport", answer: 0))
            allQuestions.append(
                QuizQuestion(questionId: "7", question: "Cooking", answer: 0))
            allQuestions.append(
                QuizQuestion(questionId: "8", question: "Hiking", answer: 0))
            allQuestions.append(
                QuizQuestion(questionId: "9", question: "Teaching", answer: 0))
            allQuestions.append(
                QuizQuestion(questionId: "10", question: "Travelling", answer: 0))
        }
        
        return allQuestions
    }
}
