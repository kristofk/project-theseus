//
//  UserProfile.swift
//  Theseus
//
//  Created by Lukas Gauster on 08/02/2019.
//  Copyright © 2019 Mirko Ricci. All rights reserved.
//

import Foundation

public class UserProfile {
    
    // Store the id of a subject and the grade for that subject
    var gradeAnswers = [String : Int]()
    
    // Store the id of all interests of the user
    var interestAnswers = [String : Int]()
    
    // Store all the answers to the personality questions
    // Key: Question Id, value: question answer
    var personalityAnswers = [String : Int]()
    
}
