//
//  AppDataManager.swift
//  Theseus
//
//  Created by Lukas Gauster on 08/02/2019.
//  Copyright © 2019 Mirko Ricci. All rights reserved.
//

import Foundation

private var _SingletonSharedInstance =  AppDataManager()

public class AppDataManager: NSObject {

    private var currentUserProfile = UserProfile()
    
    // Singleton pattern
    public class var sharedInstance : AppDataManager {
        return _SingletonSharedInstance
    }
    
    // Save questions to user profile
    func updateCurrentUserProfile(questions: [QuizQuestion], quizType: QuizType) {
        switch quizType {
        case .personality:
            // Remove old questions
            AppDataManager.sharedInstance.currentUserProfile.personalityAnswers = [String : Int]()
            for question in questions {
                AppDataManager.sharedInstance.currentUserProfile.personalityAnswers[question.questionId] = question.answer
            }
        case .grades:
            // Remove old questions
            AppDataManager.sharedInstance.currentUserProfile.gradeAnswers = [String : Int]()
            for question in questions {
                AppDataManager.sharedInstance.currentUserProfile.gradeAnswers[question.questionId] = question.answer
            }
        case .interests:
            // Remove old questions
            AppDataManager.sharedInstance.currentUserProfile.interestAnswers = [String : Int]()
            for question in questions {
                AppDataManager.sharedInstance.currentUserProfile.interestAnswers[question.questionId] = question.answer
            }
        }
    }
}
