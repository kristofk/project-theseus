//
//  QuizQuestion.swift
//  Theseus
//
//  Created by Lukas Gauster on 08/02/2019.
//  Copyright © 2019 Mirko Ricci. All rights reserved.
//

import Foundation

public struct QuizQuestion {
    let questionId: String
    var question: String
    var answer: Int
}
