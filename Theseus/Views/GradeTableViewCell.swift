//
//  GradeTableViewCell.swift
//  Theseus
//
//  Created by Lukas Gauster on 11/02/2019.
//  Copyright © 2019 Mirko Ricci. All rights reserved.
//

import UIKit

class GradeTableViewCell: UITableViewCell {
    
    let kMinValue = 1
    let kMaxValue = 10
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var gradeLabel: UILabel! {
        didSet {
            gradeLabel.layer.applySketchShadow(color: UIColor(rgba: "#000000"), alpha: 0.5, x: 3, y: 3, blur: 4, spread: 0)
            
            gradeLabel.layer.cornerRadius = 10.0
            gradeLabel.clipsToBounds = true
        }
    }
    
    var quizQuestionCellDelegate: QuizTableViewCellDelegate?
    
    var question: QuizQuestion? {
        didSet {
          setupCell()
        }
    }
    
    @IBAction func minusButtonPressed(_ sender: Any) {
        
        guard let _ = question else {
            return
        }
        
        // Decrease the value by 1
        question!.answer -= 1
        
        // Check if min was reached
        if question!.answer < kMinValue {
            question!.answer = kMinValue
        }
        
        // Update cell
        self.setupCell()
        
        // Inform delegate
        quizQuestionCellDelegate?.updatedQuestion(withId: question!.questionId, withAnswer: question!.answer)
        
    }
    
    @IBAction func plusButtonPressed(_ sender: Any) {
        
        guard let _ = question else {
            return
        }
        
        // Increment the value by 1
        question!.answer += 1
        
        // Check if max was reached
        if question!.answer > kMaxValue {
            question!.answer = kMaxValue
        }
        
        // Update cell
        self.setupCell()
        
        // Inform delegate
        quizQuestionCellDelegate?.updatedQuestion(withId: question!.questionId, withAnswer: question!.answer)
        
    }
    
    private func setupCell() {

        guard let question = question else {
            return
        }
        
        // Update title
        self.titleLabel.text = question.question
        // Update count
        gradeLabel.text = "\(question.answer)"
    }
    
    
}
