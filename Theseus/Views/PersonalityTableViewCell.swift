//
//  PersonalityTableViewCell.swift
//  Theseus
//
//  Created by Lukas Gauster on 11/02/2019.
//  Copyright © 2019 Mirko Ricci. All rights reserved.
//

import UIKit

class PersonalityTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yesButton: UIButton! {
        didSet {
            yesButton.layer.applySketchShadow(color: UIColor(rgba: "#000000"), alpha: 0.5, x: 3, y: 3, blur: 4, spread: 0)
            
            yesButton.layer.cornerRadius = 10.0
        }
    }
    
    @IBOutlet weak var noButton: UIButton! {
        didSet {
            noButton.layer.applySketchShadow(color: UIColor(rgba: "#000000"), alpha: 0.5, x: 3, y: 3, blur: 4, spread: 0)
            
            noButton.layer.cornerRadius = 10.0
        }
    }
    
    
    var quizQuestionCellDelegate: QuizTableViewCellDelegate?
    
    var question: QuizQuestion? {
        didSet {
            setupCell()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        yesButton.backgroundColor = GlobalConstants.Colors.DefaultGrayColor
        noButton.backgroundColor = GlobalConstants.Colors.DefaultGrayColor
    }
    
    
    @IBAction func didPressYesButton(_ sender: Any) {
        guard let _ = question else {
            return
        }
        
        question!.answer = 1
        
        // Update cell
        self.setupCell()
        
        // Inform delegate
        quizQuestionCellDelegate?.updatedQuestion(withId: question!.questionId, withAnswer: question!.answer)
    }
    
    @IBAction func didPressNoButton(_ sender: Any) {
        guard let _ = question else {
            return
        }
        
        question!.answer = 0
        
        // Update cell
        self.setupCell()
        
        // Inform delegate
        quizQuestionCellDelegate?.updatedQuestion(withId: question!.questionId, withAnswer: question!.answer)
    }
    
    private func setupCell() {
        
        guard let question = question else {
            return
        }
        
        // Update title
        self.titleLabel.text = question.question
        
        // Update answer
        if question.answer == 0 {
            yesButton.backgroundColor = GlobalConstants.Colors.DefaultGrayColor
            noButton.backgroundColor = GlobalConstants.Colors.PersonalityColor
        } else if question.answer == 1 {
            yesButton.backgroundColor = GlobalConstants.Colors.PersonalityColor
            noButton.backgroundColor = GlobalConstants.Colors.DefaultGrayColor
        } else {
            yesButton.backgroundColor = GlobalConstants.Colors.DefaultGrayColor
            noButton.backgroundColor = GlobalConstants.Colors.DefaultGrayColor
        }
    }
    
}
